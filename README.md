# Validatable Value Objects

Common value objects with their validation rules.

## How it Works?

Each value object (a C# record) implements the interface *IValidatable*, which exposes
a method called Validate. This method returns the tuple (*bool isValid*, *IEnumerable<string> errors*), where the errors 
are codes of type string. 

## Error Codes

### Email

| Code | Explanation |
|------|-------------|
| EmptyEmail     |    The email address is empty.         |
| InvalidEmail     |   The email doesn't match the validation regex          |

### CPF

| Code | Explanation |
|------|-------------|
| EmptyCpf | The CPF informed is empty |
| InvalidCpfLength | The CPF has a length different from 11|
| CpfConstitutedByEqualNumbers | The CPF is constituted by a string with equal characters. e.g.: 00000000000 |
| InvalidCpfVerifyingDigits | The CPF's last to digits are incorrect |

### CNPJ

| Code | Explanation |
|------|-------------|
| EmptyCnpj | The CNPJ informed is empty |
| InvalidCnpjLength | The CNPJ has a length different from 14 |
| CnpjConstitutedByEqualNumbers | The CNPJ is constituted by a string with equal characters. e.g.: 00000000000000 |
| InvalidCnpjVerifyingDigits | The CNPJ's last to digits are incorrect |


### Required Only Numbers

Since this is a generic validation, where it should be used as a base class to
other records, one must provide an error code in case of an empty string.
If this configuration is chosen, then the code "NotOnlyNumbers"
will be used in case of the string is not formed only by numbers.

On the other hand, there is a second constructor where the
developer can inform both empty and not only numbers error codes.

| Code | Explanation |
|------|-------------|
| NotOnlyNumbers | The string provided contains other characters than numbers |