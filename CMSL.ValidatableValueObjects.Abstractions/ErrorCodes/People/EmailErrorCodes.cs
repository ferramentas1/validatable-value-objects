﻿namespace CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.People
{
    public static class EmailErrorCodes
    {
        public static readonly string EmptyEmail = nameof(EmptyEmail);
        public static readonly string InvalidEmail = nameof(InvalidEmail);
    }
}