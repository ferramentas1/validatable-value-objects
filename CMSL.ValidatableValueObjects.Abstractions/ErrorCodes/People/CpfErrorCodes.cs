﻿namespace CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.People
{
    public static class CpfErrorCodes
    {
        public static readonly string EmptyCpf = nameof(EmptyCpf);
        public static readonly string InvalidCpfLength = nameof(InvalidCpfLength);
        public static readonly string CpfConstitutedByEqualNumbers = nameof(CpfConstitutedByEqualNumbers);
        public static readonly string InvalidCpfVerifyingDigits = nameof(InvalidCpfVerifyingDigits);
    }
}