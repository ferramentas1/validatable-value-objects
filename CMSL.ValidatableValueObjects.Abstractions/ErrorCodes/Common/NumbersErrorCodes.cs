﻿namespace CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.Common
{
    public static class NumbersErrorCodes
    {
        public static readonly string NotOnlyNumbers = nameof(NotOnlyNumbers);
    }
}