﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.Companies
{
    public static class CnpjErrorCodes
    {
        public static readonly string EmptyCnpj = nameof(EmptyCnpj);
        public static readonly string InvalidCnpjLength = nameof(InvalidCnpjLength);
        public static readonly string CnpjConstitutedByEqualNumbers = nameof(CnpjConstitutedByEqualNumbers);
        public static readonly string InvalidCnpjVerifyingDigits = nameof(InvalidCnpjVerifyingDigits);
    }
}
