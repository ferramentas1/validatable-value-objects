﻿using System.Collections.Generic;

namespace CMSL.ValidatableValueObjects.Abstractions.Interfaces
{
    public interface IValidatable
    {
        (bool isValid, IEnumerable<string> errors) Validate();

    }
}