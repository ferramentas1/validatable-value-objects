﻿using CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.Companies;
using CMSL.ValidatableValueObjects.ValueObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMSL.ValidatableValueObjects.ValueObjects.Companies
{
    public record Cnpj : RequiredOnlyNumbersString
    {
        public Cnpj() : base(CnpjErrorCodes.EmptyCnpj)
        {

        }

        public override (bool isValid, IEnumerable<string> errors) Validate()
        {
            var initialValidation = base.Validate();
            if (!initialValidation.isValid) return initialValidation;

            const int cnpjLength = 14;
            if (Value.Length != cnpjLength) return (false, new[] { CnpjErrorCodes.InvalidCnpjLength });

            var firstNumber = Value.First();
            if (Value.All(x => x == firstNumber)) return (false, new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers, });

            var numbers = GetAsNumberArray();
            if (numbers[^2] != GetVerifyingDigit01(numbers)) return (false, new[] { CnpjErrorCodes.InvalidCnpjVerifyingDigits });
            if( numbers[^1] != GetVerifyingDigit02(numbers)) return (false, new[] { CnpjErrorCodes.InvalidCnpjVerifyingDigits });

            return (true, Array.Empty<string>());

        }

        private int GetVerifyingDigit01(int[] numbers)
        {
            var sum = GetCnpjSum(12, numbers);
            return GetVerifyingDigit(sum);
        }

        private int GetVerifyingDigit02(int[] numbers)
        {
            var sum = GetCnpjSum(13, numbers);
            return GetVerifyingDigit(sum);
        }

        private int GetVerifyingDigit(int sum)
        {
            var module = sum % 11;
            if (module < 2) return 0;
            return 11 - module;
        }

        private int GetCnpjSum(int value, int[] numbers)
        {
            var weights = value == 12 ? GetCnpjWeightFor12Numbers() : GetCnpjWeightFor13Numbers();
            var sum = 0;
            for (var i = 0; i < value; i++)
            {
                sum += weights[i] * numbers[i];
            }

            return sum;
        }

        private static int[] GetCnpjWeightFor12Numbers() => new[] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        private static int[] GetCnpjWeightFor13Numbers() => new[] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        
        public string GetFormattedValue()
        {
            var validation = Validate();
            if (!validation.isValid) throw new InvalidOperationException("Unable to format value if it is invalid! Ensure the value is valid first.");
            
            var builder = new StringBuilder();
            builder.Append(Value[..2]);
            builder.Append('.');
            builder.Append(Value[2..5]);
            builder.Append('.');
            builder.Append(Value[5..8]);
            builder.Append('/');
            builder.Append(Value[8..12]);
            builder.Append('-');
            builder.Append(Value[12..]);

            return builder.ToString();
        }
    }
}
