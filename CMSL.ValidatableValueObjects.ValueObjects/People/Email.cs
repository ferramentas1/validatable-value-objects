﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.People;
using CMSL.ValidatableValueObjects.Abstractions.Interfaces;

namespace CMSL.ValidatableValueObjects.ValueObjects.People
{
    public record Email : IValidatable
    {
        public string Value { get; init; } = string.Empty;
        public (bool isValid, IEnumerable<String> errors) Validate()
        {
            if (string.IsNullOrEmpty(Value) || string.IsNullOrWhiteSpace(Value)) return (false, new[] {EmailErrorCodes.EmptyEmail,});

            const string emailRegex = @"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*"+
                @"@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";

            if (!Regex.IsMatch(Value, emailRegex, RegexOptions.IgnoreCase)) return (false, new[] {EmailErrorCodes.InvalidEmail,});

            return (true, Array.Empty<string>());
        }
    }
}