﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.Common;
using CMSL.ValidatableValueObjects.Abstractions.Interfaces;

namespace CMSL.ValidatableValueObjects.ValueObjects.Common
{
    public record RequiredOnlyNumbersString : IValidatable
    {
        private readonly string _emptyFieldValidationError;
        private readonly string _notOnlyNumbersValidationError;

        public string Value { get; init; } = string.Empty;

        protected RequiredOnlyNumbersString(string emptyFieldValidationError) : this(emptyFieldValidationError, NumbersErrorCodes.NotOnlyNumbers)
        {
        }
        
        protected RequiredOnlyNumbersString(string emptyFieldValidationError, string notOnlyNumbersValidationError)
        {
            _notOnlyNumbersValidationError = notOnlyNumbersValidationError;
            _emptyFieldValidationError = emptyFieldValidationError;
        }

        
        protected bool IsOnlyNumbers() => Regex.IsMatch(Value, "^[0-9]*$");
        protected int[] GetAsNumberArray() => Value.Select(c => int.Parse(c.ToString())).ToArray();

        public virtual (bool isValid, IEnumerable<string> errors) Validate()
        {
            if (string.IsNullOrEmpty(Value) || string.IsNullOrWhiteSpace(Value)) return (false, new[] {_emptyFieldValidationError});
            if (!IsOnlyNumbers()) return (false, new[] {_notOnlyNumbersValidationError});

            return (true, Array.Empty<string>());
        }
    }
}