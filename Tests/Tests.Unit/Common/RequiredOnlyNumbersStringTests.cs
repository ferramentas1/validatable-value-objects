﻿using System.Collections.Generic;
using System.Linq;
using CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.Common;
using CMSL.ValidatableValueObjects.ValueObjects.Common;
using Xunit;

namespace Tests.Unit.Common
{
    public class RequiredOnlyNumbersStringTests
    {

        [Theory]
        [MemberData(nameof(GetValidValues))]
        public void IsValidShouldReturnTrue(string value)
        {
            var sut = new FakeRequiredOnlyNumbersField
            {
                Value = value
            };
            var validationResult = sut.Validate();
            
            Assert.True(validationResult.isValid);
            Assert.Empty(validationResult.errors);
        }

        [Theory]
        [MemberData(nameof(GetInvalidValues))]
        public void ValidateShouldReturnFalse(string value, IEnumerable<string> expectedErrors)
        {
            var sut = new FakeRequiredOnlyNumbersField
            {
                Value = value
            };
            ValidateValueObject(sut, expectedErrors);
        }


        [Fact]
        public void ValidateShouldReturnFalseWithNewInvalidErrorCode()
        {
            var sut = new FakeRequiredOnlyNumbersField(FakeValidationErrors.CustomizedNotOnlyNumbers)
            {
                Value = "124a7846554fds"
            };
            ValidateValueObject(sut, new[] {FakeValidationErrors.CustomizedNotOnlyNumbers});
        }
        
        
        private static void ValidateValueObject(FakeRequiredOnlyNumbersField sut, IEnumerable<string> expectedErrors )
        {
            var validationResult = sut.Validate();
            Assert.False(validationResult.isValid);
            Assert.Equal(expectedErrors.Count(), validationResult.errors.Count());
            Assert.True(expectedErrors.All(x => validationResult.errors.Contains(x)));
        }

        public static IEnumerable<object[]> GetInvalidValues()
        {
            yield return new object[] {"abc", new[] {NumbersErrorCodes.NotOnlyNumbers}};
            yield return new object[] {",@", new[] {NumbersErrorCodes.NotOnlyNumbers}};
            yield return new object[] {"12a45", new[] {NumbersErrorCodes.NotOnlyNumbers}};
            yield return new object[] {"a123", new[] {NumbersErrorCodes.NotOnlyNumbers}};
            yield return new object[] {"145a", new[] {NumbersErrorCodes.NotOnlyNumbers}};
            yield return new object[] {"", new[] {FakeValidationErrors.EmptyFake}};
            yield return new object[] {" ", new[] {FakeValidationErrors.EmptyFake}};
            yield return new object[] {null, new[] {FakeValidationErrors.EmptyFake}};
        }

        public static IEnumerable<object[]> GetValidValues()
        {
            yield return new object[] {"123456789"};
            yield return new object[] {"012"};
            yield return new object[] {"3457"};
            yield return new object[] {"2"};
        }
    }

    internal record FakeRequiredOnlyNumbersField : RequiredOnlyNumbersString
    {
        public FakeRequiredOnlyNumbersField() : base(FakeValidationErrors.EmptyFake)
        {
        }

        public FakeRequiredOnlyNumbersField(string invalidFakeErrorCode) : base(FakeValidationErrors.EmptyFake, FakeValidationErrors.CustomizedNotOnlyNumbers)
        {
        }
    }

    internal static class FakeValidationErrors
    {
        public static string EmptyFake = nameof(EmptyFake);
        public static string CustomizedNotOnlyNumbers = nameof(CustomizedNotOnlyNumbers);

    }
}