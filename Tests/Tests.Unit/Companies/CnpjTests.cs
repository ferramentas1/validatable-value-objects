﻿using CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.Common;
using CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.Companies;
using CMSL.ValidatableValueObjects.ValueObjects.Companies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;

namespace Tests.Unit.Companies
{
    public class CnpjTests
    {

        [Theory]
        [ClassData(typeof(InvalidCnpjs))]
        public void ValidateShouldBeFalse(string value, IEnumerable<string> expectedErrors)
        {
            var sut = new Cnpj
            {
                Value = value
            };

            var (isValid, errors) = sut.Validate();
            Assert.False(isValid);
            Assert.Equal(expectedErrors.Count(), errors.Count());
            Assert.True(expectedErrors.All(x => errors.Contains(x)));
        }


        [Theory]
        [ClassData(typeof(ValidCnpjs))]
        public void ValidateShouldBeTrue(string value)
        {
            var sut = new Cnpj
            {
                Value = value
            };

            var (isValid, errors) = sut.Validate();
            Assert.True(isValid);
            Assert.Empty(errors);
        }

        [Theory]
        [ClassData(typeof(ValidCnpjs))]
        public void GetFormattedStringShouldReturnFormatedCnpj(string cnpj)
        {
            var sut = new Cnpj { Value = cnpj };
            var formattedString = sut.GetFormattedValue();
            Assert.Matches(new Regex(@"^[0-9]{2}[.][0-9]{3}[.][0-9]{3}[\/][0-9]{4}[-][0-9]{2}$"), formattedString);
        }

        [Theory]
        [ClassData(typeof(InvalidCnpjs))]
        public void GetFormattedStringShouldThrowExceptionGivenInvalidCnpj(string cnpj, IEnumerable<string> errors)
        {
            var sut = new Cnpj { Value = cnpj };
            Assert.Throws<InvalidOperationException>(() => sut.GetFormattedValue());
        }
    }


    public class InvalidCnpjs : TheoryData<string, IEnumerable<string>>
    {
        public InvalidCnpjs()
        {
            Add("11111111111111", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("22222222222222", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("33333333333333", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("44444444444444", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("55555555555555", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("66666666666666", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("77777777777777", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("88888888888888", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("99999999999999", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("00000000000000", new[] { CnpjErrorCodes.CnpjConstitutedByEqualNumbers} );
            Add("12364528000128", new[] { CnpjErrorCodes.InvalidCnpjVerifyingDigits} );
            Add("12367528000128", new[] { CnpjErrorCodes.InvalidCnpjVerifyingDigits} );
            Add("01", new[] { CnpjErrorCodes.InvalidCnpjLength } );
            Add("14587964431897497456", new[] { CnpjErrorCodes.InvalidCnpjLength } );
            Add("abc", new[] { NumbersErrorCodes.NotOnlyNumbers } );
            Add("", new[] { CnpjErrorCodes.EmptyCnpj } );
            Add(null, new[] { CnpjErrorCodes.EmptyCnpj } );
            Add(" ", new[] { CnpjErrorCodes.EmptyCnpj });
        }
    }

    public class ValidCnpjs: TheoryData<string>
    {
        public ValidCnpjs()
        {
            Add("51677818000184");
            Add("25175804000152");
            Add("69044316000181");
            Add("05049170000184");
            Add("82559728000112");
            Add("95648595000100");
            Add("41968224000179");
            Add("03022346000189");
            Add("12336936000177");
            Add("46273663000180");
            Add("01533515000110");
            Add("11222333000181");
        }
    }
}
