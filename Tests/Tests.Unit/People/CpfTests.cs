﻿using System.Collections.Generic;
using System.Linq;
using CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.Common;
using CMSL.ValidatableValueObjects.Abstractions.ErrorCodes.People;
using CMSL.ValidatableValueObjects.ValueObjects.People;
using Xunit;

namespace Tests.Unit.People
{
    public class CpfTests
    {
        [Theory]
        [MemberData(nameof(GetInvalidValues))]
        public void ValidateShouldBeFalse(string cpf, IEnumerable<string> expectedErrors)
        {
            var sut = new Cpf {Value = cpf};
            var validationResult = sut.Validate();
            Assert.False(validationResult.isValid);
            Assert.Equal(expectedErrors.Count(), validationResult.errors.Count());
            Assert.True(expectedErrors.All(x => validationResult.errors.Contains(x)));
        }

        [Theory]
        [MemberData(nameof(GetValidValues))]
        public void ValidateShouldBeTrue(string cpf)
        {
            var sut = new Cpf {Value = cpf};
            var validationResult = sut.Validate();
            Assert.True(validationResult.isValid);
            Assert.Empty(validationResult.errors);
        }

        public static IEnumerable<object[]> GetInvalidValues()
        {
            yield return new object[] {"11111111111", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"22222222222", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"33333333333", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"44444444444", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"55555555555", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"66666666666", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"77777777777", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"88888888888", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"99999999999", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] {"00000000000", new[] {CpfErrorCodes.CpfConstitutedByEqualNumbers}};
            yield return new object[] { "19068997079", new[] {CpfErrorCodes.InvalidCpfVerifyingDigits}};
            yield return new object[] { "60845825023", new[] {CpfErrorCodes.InvalidCpfVerifyingDigits}};
            yield return new object[] {"01", new[] {CpfErrorCodes.InvalidCpfLength,}};
            yield return new object[] {"14587964431897497456", new[] {CpfErrorCodes.InvalidCpfLength,}};
            yield return new object[] {"abc", new[] {NumbersErrorCodes.NotOnlyNumbers,}};
            yield return new object[] {"", new[] {CpfErrorCodes.EmptyCpf}};
            yield return new object[] {null, new[] {CpfErrorCodes.EmptyCpf}};
            yield return new object[] {" ", new[] {CpfErrorCodes.EmptyCpf}};
        }

        public static IEnumerable<object[]> GetValidValues()
        {
            yield return new object[] {"52998224725"};
            yield return new object[] {"73949818006"};
            yield return new object[] {"21770642013"};
            yield return new object[] {"16975821097"};
            yield return new object[] {"30607289090"};
            yield return new object[] {"19839346075"};
            yield return new object[] {"34196240003"};
            yield return new object[] {"11289599092"};
            yield return new object[] {"52374248054"};
            yield return new object[] {"13960019050"};
            yield return new object[] {"56518271027"};
        }
    }
}